<?php 

	add_action( 'wp_enqueue_scripts', 'enqueue_child_theme_styles', 999);

	function enqueue_child_theme_styles() {
	  wp_enqueue_style( 'stadium-child', get_stylesheet_directory_uri() . '/style.css' );
	}
 ?>
