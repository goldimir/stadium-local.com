<?php 
/*
    Template Name: Home Page
*/
?>
<?php get_header(); ?>
<div class="box-img box-img-top" style="background-image: url('<?php bloginfo(template_url); ?>/i/home_top_img.jpg');"></div>
<main>
  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <?php the_content(); ?>
  <?php endwhile; ?>
  <!-- post navigation -->
  <?php else: ?>
  <!-- no posts found -->
  <?php endif; ?>
</main>
<?php get_footer(); ?>