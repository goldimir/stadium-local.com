<?php $options = get_option( 'wpuniq_theme_options' ); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title><?php bloginfo('name'); ?></title>
    <?php wp_head(); ?>
</head>
<body>
  <!-- this comment for Sasha -->
<header>
    <div class="top-logo-menu">
        <div class="container">
            <div class="row">
        <div class="col-sm-3 col-md-2">
          <div class="logo">
              <a href="<?php echo bloginfo('home_url'); ?>"><img class="img-responsive center-block" src="<?php bloginfo(template_url); ?>/i/logo.png" /></a>
          </div>
        </div>
        <div class="col-sm-9 col-md-10">
         <nav role="navigation" class="navbar navbar-default">
            <div class="navbar-header">
            <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            </div>
            <div id="navbarCollapse" class="collapse navbar-collapse">

          <?php    /**
            * Displays a navigation menu
            * @param array $args Arguments
            */
            $args = array (
              'theme_location' => 'general-menu',
              'menu' => '',
              'container' => '',
              'container_class' => 'menu-{menu-slug}-container',
              'container_id' => '',
              'menu_class' => 'menu',
              'menu_id' => '',
              'echo' => true,
              'fallback_cb' => 'wp_page_menu',
              'before' => '',
              'after' => '',
              'link_before' => '',
              'link_after' => '',
              'items_wrap' => '<ul class = "nav navbar-nav">%3$s</ul>',
              'depth' => 0,
              'walker' => ''
            );
          
            wp_nav_menu( $args ); ?>

            </div>
          </nav>
        </div>
      </div>
    </div>
  </div>
</header>

<?php $slider = new WP_Query(array('post_type' => 'slug', 'posts_per_page' => -1)); ?>
<?php if ( $slider->have_posts()) : ?>
  
<script type="text/javascript">
    jQuery(document).ready(function ($) {

        var jssor_1_options = {
          $AutoPlay: 1,
          $Idle: 2000,
          $SlideEasing: $Jease$.$InOutSine,
          $ArrowNavigatorOptions: {
            $Class: $JssorArrowNavigator$
          },
          $BulletNavigatorOptions: {
            $Class: $JssorBulletNavigator$
          }
        };

        var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

        /*#region responsive code begin*/
        /*remove responsive code if you don't want the slider scales while window resizing*/
        function ScaleSlider() {
            var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
            if (refSize) {
                refSize = Math.min(refSize, 980);
                jssor_1_slider.$ScaleWidth(refSize);
            }
            else {
                window.setTimeout(ScaleSlider, 30);
            }
        }
        ScaleSlider();
        $(window).bind("load", ScaleSlider);
        $(window).bind("resize", ScaleSlider);
        $(window).bind("orientationchange", ScaleSlider);
        /*#endregion responsive code end*/
    });
</script>
<div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:980px;height:380px;overflow:hidden;visibility:hidden;">
<!-- Loading Screen -->
<div data-u="loading" class="jssorl-004-double-tail-spin" style="position:absolute;top:0px;left:0px;text-align:center;background-color:rgba(0,0,0,0.7);">
    <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="<?php bloginfo(template_url); ?>/i/double-tail-spin.svg" />
</div>
<div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:980px;height:380px;overflow:hidden;">

    <?php while ( $slider->have_posts()) : $slider->the_post(); ?>

       <div><?php the_post_thumbnail('full'); ?></div>

    <?php endwhile; ?>

    <a data-u="any" href="https://www.jssor.com" style="display:none">html carousel</a>
</div>
        <!-- Bullet Navigator -->
        <div data-u="navigator" class="jssorb052" style="position:absolute;bottom:12px;right:12px;" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
            <div data-u="prototype" class="i" style="width:16px;height:16px;">
                <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                    <circle class="b" cx="8000" cy="8000" r="5800"></circle>
                </svg>
            </div>
        </div>
        <!-- Arrow Navigator -->
        <div data-u="arrowleft" class="jssora053" style="width:55px;height:55px;top:0px;left:25px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
            <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
            </svg>
        </div>
        <div data-u="arrowright" class="jssora053" style="width:55px;height:55px;top:0px;right:25px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
            <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
            </svg>
        </div>
    </div>
    <!-- #endregion Jssor Slider End -->
    <?php else: ?>
     <p style="z-index: 1;visibility: initial;">No Slides Found.</p>
    <?php endif; ?>