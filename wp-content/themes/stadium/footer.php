<?php $options = get_option( 'wpuniq_theme_options' ); ?>
<footer>
  <div class="container">
    <div class="row">
      <div class="col-sm-8 col-sm-offset-2">
        <nav class="footer-navbar hidden-xs">
          <?php    /**
            * Displays a navigation menu
            * @param array $args Arguments
            */
            $args = array (
              'theme_location' => 'general-menu',
              'menu' => '',
              'container' => '',
              'container_class' => 'menu-{menu-slug}-container',
              'container_id' => '',
              'menu_class' => 'menu',
              'menu_id' => '',
              'echo' => true,
              'fallback_cb' => 'wp_page_menu',
              'before' => '',
              'after' => '',
              'link_before' => '',
              'link_after' => '',
              'items_wrap' => '<ul class = "footer-nav">%3$s</ul>',
              'depth' => 0,
              'walker' => ''
            );
          
            wp_nav_menu( $args ); ?>
        </nav>
        <div class="footer-address">
          <button>Contact Us</button>
          <?php echo $options[address];?>
          <ul class="contact-info">
              <li><a href="mailto:<?php bloginfo('admin_email'); ?>"><?php bloginfo('admin_email'); ?></a></li>
              <li><a href="tel:<?php echo str_replace(' ', '', $options[phone]); ?>"><?php echo $options[phone];?></a></li>
           </ul>
        </div>
        <div class="row footer-bottom-menu">
          <div class="col-sm-4">
            <p><?php echo $options[Copyright];?></p>
          </div>
          <div class="col-sm-8">
            <nav class="footer-bottom-navbar">
          <?php    /**
            * Displays a navigation menu
            * @param array $args Arguments
            */
            $args = array (
              'theme_location' => 'client-menu',
              'menu' => '',
              'container' => '',
              'container_class' => 'menu-{menu-slug}-container',
              'container_id' => '',
              'menu_class' => 'menu',
              'menu_id' => '',
              'echo' => true,
              'fallback_cb' => 'wp_page_menu',
              'before' => '',
              'after' => '',
              'link_before' => '',
              'link_after' => '',
              'items_wrap' => '<ul class = "footer-bottom-nav">%3$s</ul>',
              'depth' => 0,
              'walker' => ''
            );
          
            wp_nav_menu( $args ); ?>
            </nav>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>

<?php wp_footer(); ?>
</body>
</html>