<?php get_header(); ?>

<?php if ( have_posts() ) : ?>

<div class="container">
    <div class="row">   
      <div class="news-content">
          <!-- show widget --> 
      <?php // Dynamic Sidebar
      if (  is_active_sidebar( 'posts_sidebar' )  ) : ?>
        <div class="col-sm-9">

    <?php while ( have_posts() ) : the_post(); ?>
        <!-- post -->
          <?php get_template_part( 'category-blog-list'); ?>
    <?php endwhile; ?>
        <!-- post navigation -->
        </div>
        <div class="col-sm-3 wrapper-ad">

          <?php dynamic_sidebar( 'posts_sidebar' ) ?>

        </div>

      <?php else: ?>

        <div class="col-sm-12">

    <?php while ( have_posts() ) : the_post(); ?>
        <!-- post -->
          <?php get_template_part( 'category-blog-list'); ?>
    <?php endwhile; ?>
        <!-- post navigation -->
        </div>

      <?php endif; ?>
      </div>  
    </div>
</div>

<?php else: ?>
<!-- no posts found -->
<?php endif; ?>

<?php get_footer(); ?>