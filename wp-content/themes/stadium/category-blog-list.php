<div class="col-sm-6 col-md-4 nth-clear wr-post">
  <article>
    <?php 
        $thumbnail_id = get_post_thumbnail_id( $post->ID );
        $alt = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true);
        $attr = array(
            'class' => "img-responsive center-block",
            'alt' => trim(strip_tags( $alt )),
            'title' => trim(strip_tags( $alt ))
            );?>
    <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( full, $attr ); ?></a>
    <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
    <p><span><?php the_time(get_option('date_format')); ?> <?php the_time(get_option('time_format')); ?></span></p>
    <?php the_excerpt(); ?>
  </article>
</div>