<?php
add_action( 'admin_init', 'theme_options_init' );
add_action( 'admin_menu', 'theme_options_add_page' );

function theme_options_init(){
register_setting( 'wpuniq_options', 'wpuniq_theme_options');
}

function theme_options_add_page() {
add_theme_page( __( 'Theme Options', 'WP-Unique' ), __( 'Theme Options', 'WP-Unique' ), 'edit_theme_options', 'theme_options', 'theme_options_do_page' );
}

function theme_options_do_page() { global $select_options; if ( ! isset( $_REQUEST['settings-updated'] ) ) $_REQUEST['settings-updated'] = false;
?>

<div class="wrap">
	<?php screen_icon(); echo "<h2>". __( 'Theme Options', 'WP-Unique' ) . "</h2>"; ?>
	<?php if ( false !== $_REQUEST['settings-updated'] ) : ?>
	<div id="message" class="updated">
		<p><strong><?php _e( 'Saved', 'WP-Unique' ); ?></strong></p>
	</div>
	<?php endif; ?>
</div>

<form method="post" action="options.php">
	<?php settings_fields( 'wpuniq_options' ); ?>
	<?php $options = get_option( 'wpuniq_theme_options' ); ?>
	<table width="600" border="0">
<!-- 		<tr>
			<td>Logo:</td>
			<td><input type="file" name="wpuniq_theme_options[cuslogo]" id="wpuniq_theme_options[cuslogo]" /></td>
		</tr>	 -->
		<tr>
			<td>Phone:</td>
			<td><input type="tel" name="wpuniq_theme_options[phone]" id="wpuniq_theme_options[phone]" value="<?php echo $options[phone];?>" /></td>
		</tr>
		<tr>
			<td>Address:</td>
			<td><textarea name="wpuniq_theme_options[address]" id="wpuniq_theme_options[address]"><?php echo $options[address];?></textarea></td>
		</tr>
		<tr>
			<td>Copyright:</td>
			<td><input type="text" name="wpuniq_theme_options[Copyright]" id="wpuniq_theme_options[Copyright]" value="<?php echo $options[Copyright];?>" /></td>
		</tr>
		<tr>
			<td>Sidebar Position:</td>
			<td><select name="wpuniq_theme_options[sidebar_pos]" id="wpuniq_theme_options[sidebar_pos]">
				<option value="left"<?php if($options[sidebar_pos]=='left') echo ' selected="selected"';?>>Left</option>
				<option value="right"<?php if($options[sidebar_pos]=='right') echo ' selected="selected"';?>>Right</option>
			</select> </td>
		</tr>
		<tr>
			<td>Show Banner:</td>
			<td><input type="checkbox" name="wpuniq_theme_options[show_baner]" id="wpuniq_theme_options[show_baner]" value="1"<?php if($options[show_baner]=='1') echo ' checked="checked"';?> /></td>
		</tr>
		<tr>
			<td colspan="2"><input class="button button-primary button-large menu-save" type="submit" value="Save" /></td>
		</tr>
	</table>
</form>
<?php } ?>