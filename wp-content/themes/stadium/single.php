<?php get_header(); ?>
<?php if ( have_posts() ) : ?>
<main>
    <div class="container">
        <div class="row">
            <div class="news-single-post">


      <?php // Dynamic Sidebar
      if (  is_active_sidebar( 'posts_sidebar' )  ) : ?>
                <div class="col-sm-9">
        <?php while ( have_posts() ) : the_post(); ?>
                  <article>
                    <h1><?php the_title(); ?></h1>
                        <?php 
                            $thumbnail_id = get_post_thumbnail_id( $post->ID );
                            $alt = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true);
                            $attr = array(
                                'class' => "img-responsive",
                                'alt' => trim(strip_tags( $alt )),
                                'title' => trim(strip_tags( $alt ))
                                ); 
                            the_post_thumbnail( full, $attr );
                        ?>
                    <p><span><?php the_time(get_option('date_format')); ?> <?php the_time(get_option('time_format')); ?></span></p>
                    <?php the_content(); ?>
                  </article>
                  <div class="social-box">
                    <a class="btn btn-social-icon btn-facebook">
                        <span class="fa fa-facebook"></span>
                    </a>
                    <a class="btn btn-social-icon btn-twitter">
                        <span class="fa fa-twitter"></span>
                    </a>
                    <a class="btn btn-social-icon btn-linkedin">
                        <span class="fa fa-linkedin"></span>
                    </a>
                    <span class="text">Share</span>
                  </div>
                  <br><br>
        <?php endwhile; ?>
                </div>
        <div class="col-sm-3 wrapper-ad">

          <?php dynamic_sidebar( 'posts_sidebar' ) ?>

        </div>

      <?php else: ?>

                <div class="col-sm-12">
        <?php while ( have_posts() ) : the_post(); ?>
                  <article>
                    <h1><?php the_title(); ?></h1>
                        <?php 
                            $thumbnail_id = get_post_thumbnail_id( $post->ID );
                            $alt = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true);
                            $attr = array(
                                'class' => "img-responsive",
                                'alt' => trim(strip_tags( $alt )),
                                'title' => trim(strip_tags( $alt ))
                                ); 
                            the_post_thumbnail( full, $attr );
                        ?>
                    <p><span><?php the_time(get_option('date_format')); ?> <?php the_time(get_option('time_format')); ?></span></p>
                    <?php the_content(); ?>
                  </article>
                  <div class="social-box">
                    <a class="btn btn-social-icon btn-facebook">
                        <span class="fa fa-facebook"></span>
                    </a>
                    <a class="btn btn-social-icon btn-twitter">
                        <span class="fa fa-twitter"></span>
                    </a>
                    <a class="btn btn-social-icon btn-linkedin">
                        <span class="fa fa-linkedin"></span>
                    </a>
                    <span class="text">Share</span>
                  </div>
                  <br><br>
        <?php endwhile; ?>
                </div>

      <?php endif; ?>
       
        </div>
    </div>
</main>
<?php else: ?>
<!-- no posts found -->
<?php endif; ?>
<?php get_footer(); ?>