<?php 

function theme_name_scripts()
{
	wp_enqueue_style('font-opensans-rajdhani', get_template_directory_uri() . '/css/font-opensans-rajdhani.css');
	wp_enqueue_style('font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css');
	wp_enqueue_style('bootstrap.min', get_template_directory_uri() . '/css/bootstrap.min.css');
	wp_enqueue_style('style', get_template_directory_uri() . '/style.css');
	wp_enqueue_script('jquery-1.12.4', get_template_directory_uri() . '/js/jquery-1.12.4.min.js');
	wp_enqueue_script('bootstrap.min.js', get_template_directory_uri() . '/js/bootstrap.min.js');
	wp_enqueue_script('jssor.slider-25.2.0.min.js', get_template_directory_uri() . '/js/jssor.slider-25.2.0.min.js');
}

// ADD FILES IN HEADER
add_action( 'wp_enqueue_scripts', 'theme_name_scripts' );

// SHOW FEATURED IMAGES
add_theme_support('post-thumbnails');

// SHOW MENU
register_nav_menus( array(
	'general-menu' => 'General Menu',
	'client-menu' => 'Extrim Footer Menu',
) );

// ADD WIDGET OPTION IN ADMIN PANEL
	$args = array(
		'name'          => __( 'Newsroom Sidebar', 'theme_text_domain' ),
		'id'            => 'posts_sidebar',
		'description'   => '',
		'class'         => '',
		'before_widget' => '<p id="%1" class="widget %2">',
		'after_widget'  => '</p>',
		'before_title'  => '<h2 class="widgettitle">',
		'after_title'   => '</h2>'
	);

	register_sidebar( $args );

// ADD CUSTOM WIDGET RECENT POSTS
require_once( get_template_directory() . '/widgets/recent-posts.php' );
add_action( 'widgets_init', create_function( '', 'register_widget( "Stadium_Widget_Recent_Posts" );' ) );

// STOP WP REMOVING DIV TAGS
function ikreativ_tinymce_fix( $init )
{
    // html elements being stripped
    $init['extended_valid_elements'] = 'div[*], article[*]';

    // don't remove line breaks
    $init['remove_linebreaks'] = false;

    // convert newline characters to BR
    $init['convert_newlines_to_brs'] = true;

    // don't remove redundant BR
    $init['remove_redundant_brs'] = false;

    // pass back to wordpress
    return $init;
}
add_filter('tiny_mce_before_init', 'ikreativ_tinymce_fix');

// ADD ANOTHER OPTION TO ADMIN PANEL
// ADD SLIDER
function slider() {

	$labels = array(
		'name'                => __( 'Slides', 'text-domain' ),
		'singular_name'       => __( 'Singular Name', 'text-domain' ),
		'all_items'           => __( 'All Slides'),
		'add_new'             => _x( 'Add New Slide', 'text-domain', 'text-domain' ),
		'add_new_item'        => __( 'Add New Slide', 'text-domain' ),
		'edit_item'           => __( 'Edit Singular Name', 'text-domain' ),
		'new_item'            => __( 'New Slide', 'text-domain' ),
		'view_item'           => __( 'View Singular Name', 'text-domain' ),
		'search_items'        => __( 'Search Plural Name', 'text-domain' ),
		'not_found'           => __( 'No Slides found', 'text-domain' ),
		'not_found_in_trash'  => __( 'No Slides found in Trash', 'text-domain' ),
		'parent_item_colon'   => __( 'Parent Singular Name:', 'text-domain' ),
		'menu_name'           => __( 'Slider', 'text-domain' ),
	);

	$args = array(
		'labels'                   => $labels,
		'hierarchical'        => false,
		'description'         => 'description',
		'taxonomies'          => array(),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => null,
		'menu_icon'           => null,
		'show_in_nav_menus'   => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => false,
		'has_archive'         => true,
		'query_var'           => true,
		'can_export'          => true,
		'rewrite'             => true,
		'capability_type'     => 'post',
		'supports'            => array(
			'title', 'thumbnail', 'page-attributes', 'post-formats'
			)
	);

	register_post_type( 'slug', $args );
}

add_action( 'init', 'slider' );

//LENGTH OF POSTS IN CATEGORY LIST
function wpdocs_custom_excerpt_length( $length ) {
    return 20;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );

//ADD ACTIVE CLASS TO TOP MENU BAR
function custom_active_item_classes($classes = array(), $menu_item = false) {
    global $post;

    // Get post ID, if nothing found set to NULL
    $id = ( isset( $post->ID ) ? get_the_ID() : NULL );

    // Checking if post ID exist...
    if (isset( $id )){
	    $classes[] = ($menu_item->url == get_post_type_archive_link($post->post_type)) ? 'current-menu-item active' : '';
    }

    return $classes;
}
add_filter( 'nav_menu_css_class', 'custom_active_item_classes', 10, 2 );

// REPLACES THE EXCERPT "READ MORE" TEXT BY A LINK
function new_excerpt_more($more) {
       global $post;
	return ' <a class="moretag" href="'. get_permalink($post->ID) . '">Read more...</a>';
}
add_filter('excerpt_more', 'new_excerpt_more');

//ADD THEME OPTIONS
include_once('options_page.php');



?>